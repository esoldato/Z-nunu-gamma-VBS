// STL include(s):
#include <stdexcept>

// EDM include(s):
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"

// Local include(s):
#include "HGamAnalysisFramework/JetHandler.h"


namespace HG {
  SG::AuxElement::Accessor<float> JetHandler::DetectorEta("DetectorEta");
  SG::AuxElement::Accessor<std::vector<float> > JetHandler::JVF("JVF");
  SG::AuxElement::Accessor<float> JetHandler::Jvf("Jvf");
  SG::AuxElement::Accessor<float> JetHandler::CorrJvf("CorrJvf");
  SG::AuxElement::Accessor<float> JetHandler::Jvt("Jvt");
  SG::AuxElement::Accessor<float> JetHandler::Rpt("Rpt");
  SG::AuxElement::Accessor<float> JetHandler::hardVertexJvt("hardVertexJvt");
  SG::AuxElement::Accessor<char>  JetHandler::isClean("isClean");
  SG::AuxElement::Accessor<float> JetHandler::scaleFactor("scaleFactor");

  // ______________________________________________________________________________
  JetHandler::JetHandler(const char *name, xAOD::TEvent *event, xAOD::TStore *store)
    : HgammaHandler(name, event, store)
  {}

  // ______________________________________________________________________________
  JetHandler::~JetHandler()
  {}

  // ______________________________________________________________________________
  EL::StatusCode JetHandler::initialize(Config &config)
  {
    // Set appropriate return type for ANA_CHECK
    ANA_CHECK_SET_TYPE(EL::StatusCode);

    ANA_CHECK(HgammaHandler::initialize(config));

    // General options
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK(m_event->retrieve(eventInfo, "EventInfo"));

    // Read in configuration information
    m_containerName = config.getStr(m_name + ".ContainerName");
    m_correctVertex = config.getBool(m_name + ".Calibration.CorrectVertex");
    m_truthName     = config.getStr(m_name + ".TruthContainerName", "AntiKt4TruthJets");

    m_rapidityCut = config.getNum(m_name + ".Selection.MaxAbsRapidity", 4.4);
    m_ptCut       = config.getNum(m_name + ".Selection.PtPreCutGeV", 25.0) * GeV;
    m_jvf         = config.getNum(m_name + ".Selection.JVF", 0.25);
    m_jvt         = config.getNum(m_name + ".Selection.JVT", 0.64);
    m_doFJVT      = config.getBool(m_name + ".Selection.DoFJVT", true);

    // B-tagging
    m_enableBTagging = config.getBool(m_name + ".EnableBTagging", false);

    if (m_enableBTagging) {
      TString bTagCDI, bTagCDIFullPath;

      if (HG::isMC()) {
        bTagCDI = config.getStr(m_name + ".BTagging.ScaleFactorFileName" + HG::mcType());
      } else {
        bTagCDI = config.getStr(m_name + ".BTagging.DataWorkingPointFileName");
      }

      bTagCDIFullPath = PathResolverFindCalibFile(bTagCDI.Data());

      m_defaultBJetWP   = config.getStr(m_name + ".BTagging.DefaultWP", "FixedCutBEff_77");
      m_bJetEtaCut = config.getNum(m_name + ".BTagging.MaxAbsEta", 2.5);
      m_bTagNames       = config.getStrV(m_name + ".BTagging.TaggerNames");
      m_bTagEVReduction = config.getStr(m_name + ".BTagging.EVReduction");

      for (const auto &taggerName : m_bTagNames) {
        // Use the operating points defined by the Tagging tool.
        m_bTagOPs[taggerName] = config.getStrV(m_name + "." + taggerName + ".OperatingPoints");

        for (const auto &op : m_bTagOPs[taggerName]) {

          // Configure efficiency tool
          asg::AnaToolHandle<IBTaggingEfficiencyTool> effTool;
          effTool.setTypeAndName(("BTaggingEfficiencyTool/Eff_" + taggerName + "_" + op).Data());
          ANA_CHECK(effTool.setProperty("TaggerName", taggerName.Data()));
          ANA_CHECK(effTool.setProperty("OperatingPoint", op.Data()));
          ANA_CHECK(effTool.setProperty("JetAuthor", m_containerName.Data()));
          ANA_CHECK(effTool.setProperty("ScaleFactorFileName", bTagCDIFullPath.Data()));
          ANA_CHECK(effTool.setProperty("EigenvectorReductionB", m_bTagEVReduction.Data()));
          ANA_CHECK(effTool.setProperty("EigenvectorReductionC", m_bTagEVReduction.Data()));
          ANA_CHECK(effTool.setProperty("EigenvectorReductionLight", m_bTagEVReduction.Data()));

          // Set calibration for MCMC scale factors.
          // In order, these DSIDs are PowPy8EvtGen, Sherpa 2.2, PowH7, Sherpa 2.1, Py8EvtGen, Herwig7
          TString btagCalibs = "410501;410250;410558;426131;361020;364443";

          // Not all MCMC scale factors are available for all taggers so we override the default here as necessary
          // CDI file only has two options for PFlow jets
          if (m_jetAlg == "AntiKt4EMPFlow") {
            btagCalibs = "410501;default;410558;default;default;default";
            // ... and only three options for jets with continuous b-tagging
          } else if (op == "Continuous") {
            btagCalibs = "410501;410250;410558;default;default;default";
          }

          // Now set the calibration appropriately
          ANA_CHECK(effTool.setProperty("EfficiencyBCalibrations", btagCalibs.Data()));
          ANA_CHECK(effTool.setProperty("EfficiencyCCalibrations", btagCalibs.Data()));
          ANA_CHECK(effTool.setProperty("EfficiencyTCalibrations", btagCalibs.Data()));
          ANA_CHECK(effTool.setProperty("EfficiencyLightCalibrations", btagCalibs.Data()));
          ANA_CHECK(effTool.retrieve());

          // Configure selection tool
          asg::AnaToolHandle<IBTaggingSelectionTool> selTool;
          selTool.setTypeAndName(("BTaggingSelectionTool/Sel_" + taggerName + "_" + op).Data());
          ANA_CHECK(selTool.setProperty("MaxEta", m_bJetEtaCut));
          ANA_CHECK(selTool.setProperty("MinPt", m_ptCut));
          ANA_CHECK(selTool.setProperty("TaggerName", taggerName.Data()));
          ANA_CHECK(selTool.setProperty("OperatingPoint", op.Data()));
          ANA_CHECK(selTool.setProperty("JetAuthor", m_containerName.Data()));
          ANA_CHECK(selTool.setProperty("FlvTagCutDefinitionsFileName", bTagCDIFullPath.Data()));
          ANA_CHECK(selTool.retrieve());

          // Add both tools to the map
          m_bTagEffTools[taggerName].push_back(effTool);
          m_bTagSelTools[taggerName].push_back(selTool);
        }
      }
    }

    // Configure JetCalibrationTool, including origin correction from derivations if requested
    m_jetAlg = TString(m_containerName).ReplaceAll("Jets", "");
    TString jetConfig = config.getStr(m_name + ".Calibration.ConfigFile" + (HG::isAFII() ? "AFII" : ""));
    TString jetCalibSeq = config.getStr(m_name + ".Calibration.CalibSeq") + (HG::isData() ? "_Insitu" : "");
    const bool applyOriginCorrection = config.getBool("HgammaAnalysis.SelectVertex", false) && !config.getBool("HgammaAnalysis.UseHardestVertex", true);

    if (m_correctVertex && applyOriginCorrection) {
      jetCalibSeq = jetCalibSeq.ReplaceAll("Residual", "Residual_Origin");
    }

    // Initialise JetCalibrationTool
    ANA_MSG_INFO("Using jet calibration sequence: " << jetCalibSeq.Data());
    m_jetCalibTool.setTypeAndName("JetCalibrationTool/JetCalibTool");
    ANA_CHECK(m_jetCalibTool.setProperty("JetCollection", m_jetAlg.Data()));
    ANA_CHECK(m_jetCalibTool.setProperty("ConfigFile", jetConfig.Data()));
    ANA_CHECK(m_jetCalibTool.setProperty("CalibSequence", jetCalibSeq.Data()));
    ANA_CHECK(m_jetCalibTool.setProperty("IsData", HG::isData()));
    ANA_CHECK(m_jetCalibTool.setProperty("CalibArea", config.getStr(m_name + ".Calibration.CalibArea").Data()));

    if (m_correctVertex && applyOriginCorrection) {
      ANA_CHECK(m_jetCalibTool.setProperty("OriginScale", "Hgg_JetOriginConstitScaleMomentum"));
      ANA_MSG_INFO("Using jet origin scale: Hgg_JetOriginConstitScaleMomentum");
    }

    ANA_CHECK(m_jetCalibTool.retrieve());

    // Resolution and resolution smearing tools needed for MC only
    if (HG::isMC()) {
      // Jet energy resolution
      m_jerTool.setTypeAndName("JERTool/JERTool");
      ANA_CHECK(m_jerTool.setProperty("PlotFileName", PathResolverFindCalibFile(config.getStr(m_name + ".Resolution.PlotFileName").Data())));
      ANA_CHECK(m_jerTool.setProperty("CollectionName", m_containerName.Data()));
      ANA_CHECK(m_jerTool.retrieve());

      // Jet energy resolution smearing
      m_jerSmearTool.setTypeAndName("JERSmearingTool/JERSmearingTool");
      ANA_CHECK(m_jerSmearTool.setProperty("JERTool", m_jerTool));
      ANA_CHECK(m_jerSmearTool.setProperty("ApplyNominalSmearing", config.getBool(m_name + ".Resolution.ApplyNominalSmearing")));
      ANA_CHECK(m_jerSmearTool.setProperty("isMC", HG::isMC()));
      ANA_CHECK(m_jerSmearTool.setProperty("SystematicMode", config.getStr(m_name + ".Resolution.SystematicMode").Data()));
      ANA_CHECK(m_jerSmearTool.retrieve());
    }

    // Cleaning tool
    m_doCleaning  = config.getBool(m_name + ".Selection.DoCleaning", true);
    m_jetCleaningTool.setTypeAndName("JetCleaningTool/JetCleaningTool");
    ANA_CHECK(m_jetCleaningTool.setProperty("CutLevel", config.getStr(m_name + ".Selection.CutLevel").Data()));
    ANA_CHECK(m_jetCleaningTool.setProperty("DoUgly", config.getBool(m_name + ".Selection.DoUgly")));
    ANA_CHECK(m_jetCleaningTool.retrieve());

    // JES uncertainty tool
    m_jesUncTool.setTypeAndName("JetUncertaintiesTool/JetUncertaintiesTool");
    ANA_CHECK(m_jesUncTool.setProperty("JetDefinition", m_jetAlg.Data()));
    ANA_CHECK(m_jesUncTool.setProperty("MCType", config.getStr(m_name + ".Uncertainty.MCType").Data()));
    ANA_CHECK(m_jesUncTool.setProperty("ConfigFile", config.getStr(m_name + ".Uncertainty.ConfigFile").Data()));
    ANA_CHECK(m_jesUncTool.setProperty("CalibArea", config.getStr(m_name + ".Uncertainty.CalibArea").Data()));
    ANA_CHECK(m_jesUncTool.retrieve());

    // Track selection tool
    m_trackTool.setTypeAndName("InDet::InDetTrackSelectionTool/TrackSelectionTool");
    ANA_CHECK(m_trackTool.setProperty("CutLevel", "Loose"));
    ANA_CHECK(m_trackTool.retrieve());

    // JVT likelihood histogram
    m_jvtLikelihoodHist = HG::getHistogramPtrFromFile<TH2F>("JetMomentTools/JVTlikelihood_20140805.root", "JVTRootCore_kNN100trim_pt20to50_Likelihood");
    ANA_CHECK(m_jvtLikelihoodHist != nullptr);

    // JVT re-scaling tool
    m_jvtTool.setTypeAndName("JetVertexTaggerTool/JVTTool");
    ANA_CHECK(m_jvtTool.retrieve());

    // Forward JVT tool
    m_fjvtTool.setTypeAndName("JetForwardJvtTool/fJVTTool");
    ANA_CHECK(m_fjvtTool.retrieve());

    // JVT efficiency SF
    m_jvtSFTool.setTypeAndName("CP::JetJvtEfficiency/JVTSFTool");
    ANA_CHECK(m_jvtSFTool.setProperty("WorkingPoint", "Medium"));
    ANA_CHECK(m_jvtSFTool.setProperty("SFFile", config.getStr(m_name + ".JVT.ConfigFile").Data()));
    ANA_CHECK(m_jvtSFTool.setProperty("ScaleFactorDecorationName", "SF_jvt"));
    ANA_CHECK(m_jvtSFTool.setProperty("JetJvtMomentName", "Jvt"));
    ANA_CHECK(m_jvtSFTool.setProperty("JetEtaName", "DetectorEta"));
    ANA_CHECK(m_jvtSFTool.retrieve());

    // fJVT efficiency SF
    m_fjvtSFTool.setTypeAndName("CP::JetJvtEfficiency/fJVTSFTool");
    ANA_CHECK(m_fjvtSFTool.setProperty("SFFile", config.getStr(m_name + ".fJVT.ConfigFile").Data()));
    ANA_CHECK(m_fjvtSFTool.setProperty("ScaleFactorDecorationName", "SF_fjvt"));
    ANA_CHECK(m_fjvtSFTool.setProperty("JetEtaName", "DetectorEta"));
    ANA_CHECK(m_fjvtSFTool.retrieve());

    return EL::StatusCode::SUCCESS;
  }

  // ______________________________________________________________________________
  xAOD::JetContainer JetHandler::getCorrectedContainer()
  {
    // Get shallow copy from TEvent/TStore
    bool calib = false;
    xAOD::JetContainer shallowContainer = getShallowContainer(calib);
    if (calib) { return shallowContainer; }
    // Otherwise start calibrating...
    if (m_jetAlg.Contains("EM")) {
      ANA_MSG_DEBUG("Setting jet constituent state to UNCALIBRATED.");
      for (auto jet : shallowContainer) {
        jet->setConstituentsSignalState(xAOD::UncalibratedJetConstituent);
      }
    }
    // Calibrate and decorate jets
    for (auto jet : shallowContainer) {
      scaleFactor(*jet) = 1.0;
      // Apply initial decorations necessary for MxAOD
      decorateJVF(jet);
      // if (m_jetAlg.Contains("PFlow")) {
      //   ANA_MSG_DEBUG("PFlow jets cannot be cleaned. Skipping.");
      //   isClean(*jet) = true;
      // } else {
      //   // isClean(*jet) = m_jetCleaning->accept(*jet);
      //   isClean(*jet) = m_jetCleaningTool->keep(*jet);
      // }
      if (HG::isMC()) { decorateBJetTruth(jet); } // add truth-tagging for BJES unc.
      DetectorEta(*jet) = jet->getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum").eta();
      // Since calibrateJet applies systematic uncertainties, do this after decorations are applied
      calibrateJet(jet);
      // Reco b-tagging and JVT recalculation rely on the jet being fully calibrated
      recalculateJVT(*jet);
      if (m_enableBTagging) { decorateBJetReco(jet); }
    }
    // Add forward passFJVT flag, and JVT scale factors
    decorateJVTExtras(shallowContainer);
    // Some further corrections rely on the initial calibrated energy
    decorateRawCalib(shallowContainer);
    // Sort the Jets
    shallowContainer.sort(comparePt);
    return shallowContainer;
  }

  // ______________________________________________________________________________
  xAOD::JetContainer JetHandler::applySelectionNoCleaning(xAOD::JetContainer &container)
  {
    bool saveCleaning = m_doCleaning;
    m_doCleaning = false;

    xAOD::JetContainer sel = applySelection(container);

    m_doCleaning = saveCleaning;
    return sel;
  }

  // ______________________________________________________________________________
  xAOD::JetContainer JetHandler::applySelectionNoJvt(xAOD::JetContainer &container)
  {
    double saveJvt = m_jvt;
    m_jvt = -1.0;

    xAOD::JetContainer sel = applySelection(container);

    m_jvt = saveJvt;
    return sel;
  }

  // ______________________________________________________________________________
  xAOD::JetContainer JetHandler::applySelection(xAOD::JetContainer &container)
  {
    xAOD::JetContainer selected(SG::VIEW_ELEMENTS);

    for (auto jet : container) {
      // Apply pT and eta selection cuts
      if (!passPtEtaCuts(jet)) { continue; }

      // Apply cleaning cuts. MxAODs do not have isAvailable as cut is already applied
      if (m_doCleaning && isClean.isAvailable(*jet) && !isClean(*jet)) { continue; }

      // JVF cuts
      if (!passJVFCut(jet)) { continue; }

      // JVT cuts
      if (!passJVTCut(jet)) { continue; }

      // All the cuts are passed so we keep this jet
      selected.push_back(jet);
    }

    return selected;
  }

  // ______________________________________________________________________________
  xAOD::JetContainer JetHandler::applyBJetSelection(xAOD::JetContainer &container, TString wp)
  {
    if (wp == "") { wp = m_defaultBJetWP; }

    xAOD::JetContainer selected(SG::VIEW_ELEMENTS);

    if (!m_enableBTagging) { return selected; }

    for (auto jet : container) {
      if (jet->auxdata<char>(wp.Data())) { selected.push_back(jet); }
    }

    return selected;
  }

  // ______________________________________________________________________________
  CP::SystematicCode JetHandler::applySystematicVariation(const CP::SystematicSet &sys)
  {
    // Set appropriate return type for ANA_CHECK
    ANA_CHECK_SET_TYPE(CP::SystematicCode);

    setVertexCorrected(false);

    bool isAffected = false;

    for (auto var : sys) {
      if (m_jesUncTool->isAffectedBySystematic(var)) {
        isAffected = true;
        break;
      }

      if (m_jvtSFTool->isAffectedBySystematic(var)) {
        isAffected = true;
        break;
      }

      if (m_fjvtSFTool->isAffectedBySystematic(var)) {
        isAffected = true;
        break;
      }

      if (HG::isMC()) {
        if (m_jerSmearTool->isAffectedBySystematic(var)) {
          isAffected = true;
          break;
        }

        for (auto name : m_bTagNames) {
          for (auto tool : m_bTagEffTools[name]) {
            if (tool->isAffectedBySystematic(var)) {
              isAffected = true;
              break;
            }
          }
        }
      }
    }

    // This should mean that the jets can be shifted by this systematic
    if (isAffected) {
      ANA_CHECK(m_jesUncTool->applySystematicVariation(sys));
      ANA_CHECK(m_jvtSFTool->applySystematicVariation(sys));
      ANA_CHECK(m_fjvtSFTool->applySystematicVariation(sys));

      if (HG::isMC()) {
        ANA_CHECK(m_jerSmearTool->applySystematicVariation(sys));

        for (auto name : m_bTagNames) {
          for (auto tool : m_bTagEffTools[name]) {
            ANA_CHECK(tool->applySystematicVariation(sys));
          }
        }
      }

      m_sysName = (sys.name() == "" ? "" : "_" + sys.name());

      // Jets are not affected by this systematic
    } else {
      ANA_CHECK(m_jesUncTool->applySystematicVariation(CP::SystematicSet()));
      ANA_CHECK(m_jvtSFTool->applySystematicVariation(CP::SystematicSet()));
      ANA_CHECK(m_fjvtSFTool->applySystematicVariation(CP::SystematicSet()));

      if (HG::isMC()) {
        ANA_CHECK(m_jerSmearTool->applySystematicVariation(CP::SystematicSet()));

        for (auto name : m_bTagNames) {
          for (auto tool : m_bTagEffTools[name]) {
            ANA_CHECK(tool->applySystematicVariation(CP::SystematicSet()));
          }
        }
      }

      m_sysName = "";
    }

    return CP::SystematicCode::Ok;
  }

  // ______________________________________________________________________________
  void JetHandler::calibrateJet(xAOD::Jet *jet)
  {
    // applies calibration to a jet
    double E_before = jet->e();

    m_jetCalibTool->applyCalibration(*jet).ignore();

    if (HG::isMC()) {
      m_jerSmearTool->applyCorrection(*jet).ignore();
    }

    // Uncertainty shifting
    if ((jet->pt() > 20.0 * HG::GeV) && (fabs(jet->rapidity()) < 4.4)) {
      m_jesUncTool->applyCorrection(*jet).ignore();
    }

    // decorate the photon with the calibration factor
    jet->auxdata<float>("Ecalib_ratio") = jet->e() / E_before;
  }

  // ______________________________________________________________________________
  bool JetHandler::passPtEtaCuts(const xAOD::Jet *jet)
  {
    // eta cuts
    if (fabs(jet->rapidity()) > m_rapidityCut) { return false; }

    // pt cuts
    if (jet->pt() < m_ptCut) { return false; }

    return true;
  }

  // ______________________________________________________________________________
  void JetHandler::decorateRescaledJVT(xAOD::Jet &jet)
  {
    Jvt(jet) = m_jvtTool->updateJvt(jet);
  }

  // ______________________________________________________________________________
  void JetHandler::recalculateJVT(xAOD::Jet &jet)
  {
    // Always decorate the hardVertex value (used by MET maker)
    hardVertexJvt(jet) = m_jvtTool->updateJvt(jet);

    // Retrieve HGamVertices, using default JVT if this fails
    ConstDataVector<xAOD::VertexContainer> *vertices = nullptr;

    if (m_store->retrieve(vertices, "HGamVertices").isFailure()) {
      ANA_MSG_WARNING("Failed to retrieve HGamVertices from TStore. Using default JVT value.");
      decorateRescaledJVT(jet);
      return;
    }

    // Find selected vertex, using default JVT if this fails
    const xAOD::Vertex *diphoton_vertex = nullptr;

    if (vertices && (vertices->size() > 0)) { diphoton_vertex = vertices->at(0); }

    if (diphoton_vertex == nullptr) {
      ANA_MSG_WARNING("Selected diphoton vertex could not be determined. Using default JVT value.");
      decorateRescaledJVT(jet);
      return;
    }

    // Get track container
    const xAOD::TrackParticleContainer *tracks = nullptr;

    if (m_event->retrieve(tracks, "InDetTrackParticles").isFailure()) {
      ANA_MSG_FATAL("Failed to retrieve InDetTrackParticles, exiting.");
      throw std::invalid_argument("Failed to retrieve InDetTrackParticles, exiting.");
    }

    // Check tracks, using default JVT if this fails
    if (tracks == nullptr) {
      ANA_MSG_WARNING("InDetTrackParticles retrieved as a nullptr! Using default JVT value.");
      decorateRescaledJVT(jet);
      return;
    }

    // Count the number of pileup tracks
    int nPileupTracks = 0;

    for (auto track : *tracks) {
      if (track == nullptr) { continue; }

      if (m_trackTool->accept(*track, diphoton_vertex) && track->vertex() && track->vertex() != diphoton_vertex && (track->pt() < 30e3)) {
        nPileupTracks++;
      }
    }

    if (nPileupTracks == 0) { nPileupTracks = 1; }

    // Get all tracks ghost-associated to the jet
    std::vector<const xAOD::IParticle *> jetTracks;
    jet.getAssociatedObjects<xAOD::IParticle>(xAOD::JetAttribute::GhostTrack, jetTracks);

    // Iterate over ghost-tracks, rejecting invalid ones and calculating sum(pT)
    double ptSum_all(0.0), ptSum_pv(0.0), ptSum_pu(0.0);

    for (auto jetTrack : jetTracks) {
      if (jetTrack == nullptr) { continue; }

      const xAOD::TrackParticle *track = static_cast<const xAOD::TrackParticle *>(jetTrack);

      // Add accepted tracks to pT sums for all, primary vertex, pileup
      if (m_trackTool->accept(*track, diphoton_vertex) && track->pt() > 500) {
        ptSum_all += track->pt();

        if (track->vertex() == diphoton_vertex || (!track->vertex() && (fabs((track->z0() + track->vz() - diphoton_vertex->z()) * sin(track->theta())) < 3.0))) {
          ptSum_pv += track->pt();
        }

        if (track->vertex() && track->vertex() != diphoton_vertex) {
          ptSum_pu += track->pt();
        }
      }
    }

    // Perform explicit JVT calculation
    double _Rpt     = ptSum_pv / jet.pt();
    double _CorrJVF = ptSum_pv + ptSum_pu > 0 ? ptSum_pv / (ptSum_pv + 100 * ptSum_pu / nPileupTracks) : -1;
    double _JVT     = _CorrJVF >= 0 ? m_jvtLikelihoodHist->Interpolate(_CorrJVF, std::min(_Rpt, 1.0)) : -0.1;
    Rpt(jet) = _Rpt;
    CorrJvf(jet) = _CorrJVF;
    Jvt(jet) = _JVT;
  }

  //______________________________________________________________________________
  double JetHandler::multiplyJvtWeights(const xAOD::JetContainer *jets_noJvtCut)
  {
    // Static function so can't use asg::Messaging
    static SG::AuxElement::Decorator<float> SF_jvt("SF_jvt");

    double weight = 1.0;

    for (auto jet : *jets_noJvtCut) {
      float current_sf = SF_jvt(*jet);

      if (current_sf <= 0.0) {
        Warning("JetHandler::multiplyJvtWeights", "Found a jet which failed JVT calibration, skipping!");
        continue;
      }

      weight *= current_sf;
    }

    return weight;
  }

  //______________________________________________________________________________
  double JetHandler::multiplyFJvtWeights(const xAOD::JetContainer *jets_noJvtCut)
  {
    // Static function so can't use asg::Messaging
    static SG::AuxElement::Decorator<float> SF_fjvt("SF_fjvt");

    double weight = 1.0;

    for (auto jet : *jets_noJvtCut) {
      float current_sf = SF_fjvt(*jet);

      if (current_sf <= 0.0) {
        // Warning("JetHandler::multiplyFJvtWeights", "Found a jet which failed fJVT calibration, skipping!");
        continue;
      }

      weight *= current_sf;
    }

    return weight;
  }

  // ______________________________________________________________________________
  void JetHandler::decorateJVTExtras(xAOD::JetContainer &jets)
  {
    // Now decorate JVT and fJVT scale factors
    static SG::AuxElement::Decorator<float> SF_jvt("SF_jvt");
    static SG::AuxElement::Decorator<float> SF_fjvt("SF_fjvt");
    float _unused;

    // Calling this decorates isJvtHS and SF_jvt
    if (HG::isMC()) {
      m_jvtSFTool->applyAllEfficiencyScaleFactor(&jets, _unused);
    } else {
      for (auto jet : jets) { SF_jvt(*jet) = 1.0; }
    }

    // Tag jets with "passFJVT" forward JVT decoration
    if (m_doFJVT) {
      m_fjvtTool->modify(jets);
    }

    // Calling this decorates SF_fjvt
    if (HG::isMC() && m_doFJVT) {
      m_fjvtSFTool->applyAllEfficiencyScaleFactor(&jets, _unused);
    } else {
      for (auto jet : jets) { SF_fjvt(*jet) = 1.0; }
    }
  }

  // ______________________________________________________________________________
  float JetHandler::weightJvt(const xAOD::JetContainer &jets)
  {
    if (HG::isData()) { return 1.0; }

    float weight = 1.0;
    m_jvtSFTool->applyAllEfficiencyScaleFactor(&jets, weight);
    return weight;
  }

  // ______________________________________________________________________________
  float JetHandler::weightFJvt(const xAOD::JetContainer &jets)
  {
    if (HG::isData()) { return 1.0; }

    float weight = 1.0;
    m_fjvtSFTool->applyAllEfficiencyScaleFactor(&jets, weight);
    return weight;
  }

  // ______________________________________________________________________________
  bool JetHandler::passJVTCut(const xAOD::Jet *jet)
  {
    // Normal jet check
    if (m_jvt < 0) {
      return true;
    }

    if ((jet->pt() > 60.0 * HG::GeV) || (fabs(DetectorEta(*jet)) > 2.4) || m_jvtSFTool->passesJvtCut(*jet)) {
      return true;
    }

    return false;
  }

  // ______________________________________________________________________________
  bool JetHandler::passJVFCut(const xAOD::Jet *jet, bool useBTagCut)
  {
    // If the cut is disabled, just return true
    if (m_jvf < 0) { return true; }

    float jvf = useBTagCut ? m_bJetJvfCut : m_jvf;

    if ((jet->pt() < 50.0 * HG::GeV) && (fabs(jet->eta()) < 2.4) && (fabs(Jvf(*jet)) < jvf)) {
      return false;
    }

    return true;
  }

  // ______________________________________________________________________________
  void JetHandler::decorateJVF(xAOD::Jet *jet)
  {
    // JVF decoration
    const xAOD::VertexContainer *vertices = 0;

    if (!m_event->retrieve(vertices, "PrimaryVertices").isSuccess()) {
      ANA_MSG_FATAL("Failed to retrieve PrimaryVertices, exiting.");
      throw std::invalid_argument("Failed to retrieve PrimaryVertices, exiting.");
    }

    size_t pv = 0, npv = vertices->size();

    for (; pv < npv; ++pv) {
      if ((*vertices)[pv]->vertexType() == xAOD::VxType::VertexType::PriVtx) {
        if (JVF.isAvailable(*jet)) {
          Jvf(*jet) = JVF(*jet).at(pv);
        } else {
          Jvf(*jet) = -99;
        }

        return;
      }
    }

    Jvf(*jet) = -1.0;
  }

  // ______________________________________________________________________________
  TString JetHandler::getFlavorLabel(xAOD::Jet *jet)
  {
    static SG::AuxElement::ConstAccessor<int> PartonTruthLabelID("PartonTruthLabelID");
    int truthID = PartonTruthLabelID(*jet);

    if (truthID == 5)  { return "B"; }

    if (truthID == 4)  { return "C"; }

    if (truthID == 15) { return "T"; }

    return "Light";
  }


  // ______________________________________________________________________________
  void JetHandler::setMCGen(TString sampleName)
  {
    m_mcIndex = 0;  // Default: Pythia8

    if (sampleName.Contains("Pythia"))          { m_mcIndex = 0; }
    else if (sampleName.Contains("Py8"))        { m_mcIndex = 0; }
    else if (sampleName.Contains("PowhegPy"))   { m_mcIndex = 0; }
    else if (sampleName.Contains("Sherpa"))     { m_mcIndex = 1; }
    else if (sampleName.Contains("Hwpp"))       { m_mcIndex = 2; }
    else if (sampleName.Contains("PowhegH7"))   { m_mcIndex = 2; }
    // else if (sampleName.Contains("Sherpa"))     { m_mcIndex = 3; } // Sherpa 2.1 dijets -- not recommended by flavour tagging group
    // else if (sampleName.Contains("Pythia8"))    { m_mcIndex = 2; } // Pythia 8 dijets -- not recommended by flavour tagging group
    // else if (sampleName.Contains("Herwig7"))    { m_mcIndex = 4; } // Herwig 7 dijets -- not recommended by flavour tagging group
    else {
      ANA_MSG_WARNING("Could not identify generator (or identified an unsupported generator) for MCMC SFs. Will use Pythia8.");
    }

    // Print message identifying showering
    if (m_mcIndex == 0)  { ANA_MSG_INFO(sampleName.Data() << " identified as Pythia8 showering."); }
    else if (m_mcIndex == 1)  { ANA_MSG_INFO(sampleName.Data() << " identified as Sherpa showering."); }
    else if (m_mcIndex == 2)  { ANA_MSG_INFO(sampleName.Data() << " identified as Herwig++/Herwig7 showering."); }
  }


  // ______________________________________________________________________________
  void JetHandler::decorateBJetReco(xAOD::Jet *jet)
  {
    // Jets always need to be decorated with something, for writting to ROOT files
    for (auto name : m_bTagNames) {
      for (auto op : m_bTagOPs[name]) {
        jet->auxdata<char>((name + "_" + op).Data()) = false;
        jet->auxdata<float>(("Eff_" + name + "_" + op).Data()) = 1.0;
        jet->auxdata<float>(("InEff_" + name + "_" + op).Data()) = 1.0;
        jet->auxdata<float>(("SF_" + name + "_" + op).Data()) = 1.0;

      }
    }

    // Also initialise selected discriminants
    const std::vector<std::string> discriminantNames = {"MV2c10", "MV2c100", "MV2cl100"};

    for (const auto &discriminantName : discriminantNames) {
      jet->auxdata<double>(discriminantName + "_discriminant") = -99;
    }

    // Ensure the jet is inside the rapidity region
    if (fabs(jet->eta()) >= m_bJetEtaCut) { return; }

    // Require JVT cut for MC15 tagging
    if (!passJVTCut(jet)) { return; }

    // Fill the discrimants
    double tagging_discriminant(-99);

    for (const auto &discriminantName : discriminantNames) {
      tagging_discriminant = -99;

      if (jet->btagging() == 0 or not jet->btagging()->MVx_discriminant(discriminantName, tagging_discriminant)) {
        ANA_MSG_WARNING("Couldn't retrieve value of " + discriminantName + " discriminant!");
      } else {
        jet->auxdata<double>(discriminantName + "_discriminant") = tagging_discriminant;
      }
    }

    // Calculate b-tagging scale factors
    float btagSF(1.0);

    for (auto taggerName : m_bTagNames) {
      for (size_t i = 0; i < m_bTagOPs[taggerName].size(); ++i) {
        if (m_bTagSelTools[taggerName].at(i)->accept(*jet)) {
          jet->auxdata<char>((taggerName + "_" + m_bTagOPs[taggerName].at(i)).Data()) = true;
        }

        if (HG::isMC()) {
          // Set MCMC
          if (not m_bTagEffTools[taggerName].at(i)->setMapIndex(getFlavorLabel(jet).Data(), m_mcIndex)) {
            ANA_MSG_WARNING("Couldn't set MC/MC index properly. Results will be biased.");
          }

          // Decorate with efficiency
          btagSF = 1.0;

          if (m_bTagEffTools[taggerName].at(i)->getEfficiency(*jet, btagSF) == CP::CorrectionCode::Ok) {
            jet->auxdata<float>(("Eff_" + taggerName + "_" + m_bTagOPs[taggerName].at(i)).Data()) = btagSF;
          }

          // Decorate with inefficiency
          btagSF = 1.0;

          if (m_bTagEffTools[taggerName].at(i)->getInefficiency(*jet, btagSF) == CP::CorrectionCode::Ok) {
            jet->auxdata<float>(("InEff_" + taggerName + "_" + m_bTagOPs[taggerName].at(i)).Data()) = btagSF;
          }

          // Decorate with the correct SF depending on whether the jet is tagged or untagged.
          // ... for continuous b-tagging we will always use "passed"
          btagSF = 1.0;
          CP::CorrectionCode retrievedSF = (m_bTagSelTools[taggerName].at(i)->accept(*jet) || m_bTagOPs[taggerName].at(i).Contains("Continuous")) ?
                                           m_bTagEffTools[taggerName].at(i)->getScaleFactor(*jet, btagSF) :
                                           m_bTagEffTools[taggerName].at(i)->getInefficiencyScaleFactor(*jet, btagSF);

          if (retrievedSF == CP::CorrectionCode::Ok) {
            jet->auxdata<float>(("SF_" + taggerName + "_" + m_bTagOPs[taggerName].at(i)).Data()) = btagSF;
          }
        }
      }
    }
  }

  // ______________________________________________________________________________
  void JetHandler::decorateBJetTruth(xAOD::Jet *jet)
  {
    if (HG::isData()) {
      ANA_MSG_FATAL("Only MC jets can be decorated with truth-b-tagging information!");
      throw std::invalid_argument("Only MC jets can be decorated with truth-b-tagging information!");
    }

    // If this is MC, then add the isBjet decoration for use by BJES
    SG::AuxElement::Accessor<char> accIsBjet("IsBjet");
    accIsBjet(*jet) = (jet->auxdata<int>("HadronConeExclTruthLabelID") == 5);
  }

  // ______________________________________________________________________________
  void JetHandler::printJet(const xAOD::Jet *jet, TString comment)
  {
    // Static function so need to use ROOT::Info instead of use asg::Messaging
    Info("JetHandler::printJet", "Jet %2zu  %s", jet->index(), comment.Data());
    Info("JetHandler::printJet", "   (pT,eta,phi,m) = (%5.1f GeV,%6.3f,%6.3f,%4.1f GeV)", jet->pt() / GeV, jet->eta(), jet->phi(), jet->m() / GeV);

    // print some more information
    TString str;

    if (jet->isAvailable<float>("Ecalib_ratio")) {
      str += Form("   calibFactor = %.3f", jet->auxdata<float>("Ecalib_ratio"));
    }

    Info("JetHandler::printJet", str);
  }
}
