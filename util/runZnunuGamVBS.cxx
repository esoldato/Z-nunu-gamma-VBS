#include "ZnunuGamVBS_pack/ZnunuGamVBS.h"
//#include "HGamAnalysisFramework/RunUtils.h"
#include "ZnunuGamVBS_pack/MyRunUtils.h"

int main(int argc, char *argv[])
{
  // Set up the job for xAOD access
  xAOD::Init().ignore();

  // Create our algorithm
  ZnunuGamVBS *alg = new ZnunuGamVBS("ZnunuGamVBS");

  // Use helper to start the job
  HG::MyRunJob(alg, argc, argv);

  return 0;
}
