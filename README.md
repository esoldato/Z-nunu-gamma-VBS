#!!!!!!!for those who already have the framework setup: you need to switch to master version of HGamCore package 

#how to do it

cd HGam

#to switch to the new analysis base you need to delete build folder

rm -rf ./build

mkdir build

cd source

setupATLAS

asetup AnalysisBase,21.2,latest,here 

cd HGamCore

git checkout -- HGamAnalysisFramework/Root/ElectronHandler.cxx #restoring original version of the file

git checkout -- HGamAnalysisFramework/Root/PhotonHandler.cxx #restoring original version of the file

git checkout -- HGamAnalysisFramework/Root/JetHandler.cxx #restoring original version of the file

git checkout master

git pull

git submodule update --init --recursive

cd -

source HGamCore/HGamAnalysisFramework/scripts/setupCMake 

cd $TestArea/../build 

cmake ../source

cmake --build .

source $TestArea/../build/$AnalysisBase_PLATFORM/setup.sh 

#don't forget to update ZnunuGamVBS_pack and recompile!

#also you still need to replace photonHandler.cxx: 

cd $TestArea/ZnunuGamVBS_pack

cp PhotonHandler.cxx $TestArea/HGamCore/HGamAnalysisFramework/Root/.

#!!!!!!!!for those who already have the framework setup

#first you need to setup and configure git as instructed here https://atlassoftwaredocs.web.cern.ch/gittutorial/env-setup/

#full git tutorial can be found here https://atlassoftwaredocs.web.cern.ch/gittutorial/

#This instruction is made for lxplus. If you need to run in on a local machine do following:

kinit ${LXPLUS_USERNAME}@CERN.CH
#(not tried yet)

#You might also need the below

aklog -c cern.ch -k CERN.CH 
#(not tried yet)

#For initial HGam Framework installation do following:

mkdir HGam 

cd HGam 

mkdir build run source 

cd source

setupATLAS 

#Attention! There was a switch to the master version of HGamCore package, which requires newer Analysis base 21.2.52 (previously 21.2.32)

asetup AnalysisBase,21.2,latest,here 

cd $TestArea # this is the "source" directory that you created earlier 

git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-hgam-sw/HGamCore.git 

cd HGamCore 

# you don't need "git checkout v1.7.0-h023" step anymore, since we're switching to the master version

git submodule update --init --recursive 

cd - 

source HGamCore/HGamAnalysisFramework/scripts/setupCMake 

cd $TestArea/../build 

cmake ../source

cmake --build .

source $TestArea/../build/$AnalysisBase_PLATFORM/setup.sh 

#Note: when switching to a new version of AnalysisBase you will likely need to delete the build/ directory and re-make it.

#Now cloning this project

cd $TestArea/

#this is an example of the link. it is necessary to fork the ZnunuGamVBS project to your git lab profile and use the link from the forked project

git clone https://:@gitlab.cern.ch:8443/$USER/Z-nunu-gamma-VBS.git ZnunuGamVBS_pack

cd ZnunuGamVBS_pack

git remote add upstream https://:@gitlab.cern.ch:8443/akurova/Z-nunu-gamma-VBS.git

git fetch upstream master #bring you local clone up-to-date with the main repository

#!!!!!!!!!!!!!!!! Temporary !!!!!!!!!!!!!!!!!!

cp PhotonHandler.cxx $TestArea/HGamCore/HGamAnalysisFramework/Root/. #(necessary for dealing with missing photon shower shape variables in current version of EXOT6 derivation)

#!!!!!!!!!!!!!!!! Temporary !!!!!!!!!!!!!!!!!!

# after cloning the project and every time you add new package do (this is instead "rc find packages" and "rc compile"):

cd $TestArea/../build 

cmake ../source 

cmake --build .

#just to compile changed code use 

cd $TestArea/../build 

cmake --build .

#or if you changed only ZnunuGamVBS_pack use 

cmake --build ZnunuGamVBS_pack # inside build directory

#every time before starting the work do this (the same commands are in the setup.sh of ZnunuGamVBS_pack, so one can use the script instead):

cd HGam/source # or wherever you made your area

setupATLAS

asetup --restore 

source $TestArea/../build/$AnalysisBase_PLATFORM/setup.sh # this creates environment variables

#to run locally do runZnunuGamVBS path-to-config path-to-file:

runZnunuGamVBS $TestArea/ZnunuGamVBS_pack/data/ZnunuGamVBS.config path/to/file

#to run on the grid do (nc_nFiles and nc_mergeOutput are optional):

runZnunuGamVBS $TestArea/ZnunuGamVBS_pack/data/ZnunuGamVBS.config GridDS: mc16_13TeV.364519.Sherpa_222_NNPDF30NNLO_nunugamma_pty_140_E_CMS.deriv.DAOD_EXOT6.e5928_s3126_r9364_r9315_p3596 OutputDS: myAnalysisOutputDS UserName: your_user_name nc_nFiles: 5 nc_mergeOutput: 1

# more options for GRID running are available now. Use "*" for searching DSs using rucio, lists for the GRID samples are also available now (NOTICE: use separate lists for data and MC, they can be used simultaniously)

#for syntaxis and all running options run runZnunuGamVBS without any arguments or see MyRunUtils.cxx

#useful git commands

git pull # this is svn update analogue

git status # to show which files are changed and which are staged for commit

git log # commits history

git add # this is svn add analogue, however, you need to specify in each commit which changes you want to add anyway

git commit -a -m "commit details" # -a means that all changed files will be added to the commit. If you created the new file, you need to make "git add file" before that.

git push -u origin master # uploading your local commits to the server


#you don't need to do this if you have the package already
#To create new algorithm ZnunuGamVBS and package ZnunuGamVBS_pack with all functions inc. PostExecute() and Finalize():

make_hgam_pkg ZnunuGamVBS_pack ZnunuGamVBS --all
