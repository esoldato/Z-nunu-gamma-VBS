#ifndef ZnunuGamVBS_pack_ZnunuGamVBS_H
#define ZnunuGamVBS_pack_ZnunuGamVBS_H

#include "HGamAnalysisFramework/HgammaAnalysis.h"
#include "ZnunuGamVBS_pack/MyOverlap.h"

#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

#include "PhotonVertexSelection/PhotonPointingTool.h"
// #include "TauAnalysisTools/TauSelectionTool.h"
// #include "TauAnalysisTools/TauSmearingTool.h"
// #include "TauAnalysisTools/TauTruthMatchingTool.h"
// #include "xAODTau/TauJetContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODCore/ShallowAuxContainer.h"
#include "PhotonVertexSelection/PhotonVertexHelpers.h"

class ZnunuGamVBS : public HgammaAnalysis
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  
  //Tools

  AsgPhotonIsEMSelector* m_photonTightIsEMSelectorMC15; //!
  const CP::PhotonPointingTool* pointTool; //!
  // TauAnalysisTools::TauSelectionTool* m_tauSelTool; //!
  // TauAnalysisTools::TauSmearingTool* m_tauSmeTool; //!

  TTree *m_tree; //!
  TTree *m_tree_sw; //!
  TTree *m_tree_ev; //!
  TTree *m_fr_eg_tree; //!
  TTree *m_fr_ee_tree; //!
  TTree *m_emet_tree; //!
  TTree *m_tr_tree; //!
  xAOD::TEvent *m_event; //!
  xAOD::TStore* m_store; //!
  // const xAOD::TauJetContainer* m_taus=0;
  // xAOD::TauJetContainer* m_newTaus=0;
  // TauAnalysisTools::TauTruthMatchingTool * m_t2MT;  //!
  // std::pair< xAOD::TauJetContainer*, xAOD::ShallowAuxContainer* > m_TauJetContShallowCopy;
  double m_ph_pT, m_sel_ph_pT, m_el_pT, m_sel_el_pT, m_mu_pT, m_sel_mu_pT, m_jet_pT, m_sel_jet_pT, ph_z_pointed, m_sumw_DxAOD, m_sumw_xAOD, sum_of_weights, maxDcycle, maxAcycle; 
  unsigned int n_ph, n_ph_tight, n_ph_all, n_e_looseBL, n_e_medium, n_mu, n_jet, n_e_looseBL_ptcut, n_tau;
  unsigned int m_ph_isEM_tight_mc15, fr_ee, fr_ey, emet; //! isEM word - new tight (mc15)
  int ph_convFlag, mc_ph_origin, mc_ph_type; //!
  bool ph_tight, do_calib; //!
  double ph_pt, ph_eta, ph_phi, ph_iso_pt, ph_iso_et40, ph_iso_et20, jet_sphericity, jet_sum_pt, jet_lead_pt, jet_lead_eta, jet_lead_phi, jet_lead_E, jet_sublead_pt, jet_sublead_eta, \
 jet_sublead_phi, jet_sublead_E, metCST_pt, metCST_phi, metTST_pt, metTST_phi, e_lead_pt, e_lead_eta, e_lead_phi, e_lead_E, e_sublead_pt, e_sublead_eta, e_sublead_phi, e_sublead_E, \
 Weight; //!
  TLorentzVector vec, lead_ph, lead_jet, lead_el, sublead_neutr, lead_neutr, neutr_syst, tr_lead_jet, tr_slead_jet; //!
  unsigned int m_ph, m_sel_ph, m_el, m_sel_el, m_mu, m_sel_mu, m_jets, m_sel_jet, RunNumber, EventNumber;
  unsigned int m_ph_N, m_sel_ph_N, m_el_N, m_sel_el_N, m_mu_N, m_sel_mu_N, m_jet_N, m_sel_jet_N; //!

//true
  unsigned int tr_ph_n,tr_Z_n, tr_mu_n, tr_el_n,tr_jet_n; //!
  double tr_ph_pt, tr_ph_eta, tr_ph_phi, tr_ph_E, tr_ph_etcone40, tr_ph_ptcone20, tr_Z_pt, tr_Z_phi, tr_jet_pt, tr_jet_eta, tr_jet_phi, tr_jet_E, tr_sl_jet_pt, tr_sl_jet_eta, 
  tr_sl_jet_phi, tr_sl_jet_E, tr_jet_sum_pt, tr_Weight, sum_of_weights1, weight_cor; //!


#ifndef __CINT__
  HG::MyOverlap *m_MyOverlap; //!
  // HG::Config* my_conf;
#endif // __CINT__

protected:
  inline virtual HG::MyOverlap* overlapMine() {return m_MyOverlap;}

public:
  // this is a standard constructor
  ZnunuGamVBS() { }
  ZnunuGamVBS(const char *name);
  virtual ~ZnunuGamVBS();

  // these are the functions inherited from HgammaAnalysis
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode execute();

  // these are all the other functions inherited from the Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(ZnunuGamVBS, 1);
};

#endif // ZnunuGamVBS_pack_ZnunuGamVBS_H
