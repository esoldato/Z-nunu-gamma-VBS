#include "ZnunuGamVBS_pack/MyOverlap.h"
  
  namespace HG {
  //______________________________________________________________________________
  MyOverlap::MyOverlap(TString name)
  : m_name(name) { }
  
  //______________________________________________________________________________
  MyOverlap::~MyOverlap() { }
  
  //______________________________________________________________________________
  EL::StatusCode MyOverlap::initialize(Config &conf)
  {

    // Read the matching method (y,phi) or (eta,phi) DR matching
    TString matchStr = conf.getStr(m_name+".MatchingMode","RapidityPhi");
    if      (matchStr=="RapidityPhi") m_matchMode=y_phi;
    else if (matchStr=="EtaPhi") m_matchMode=eta_phi;
    else HG::fatal("Can not interpret MatchingMode "+matchStr);
    
    // Read in the DeltaR distances used in the overlap removal
    // Default values are from the Run 1 analysis.
    m_DR_e_mu  = conf.getNum(m_name+".Electron_DR_Muon",0.1);
    m_DR_y_e  = conf.getNum(m_name+".Photon_DR_Electron",0.4);
    m_DR_y_mu  = conf.getNum(m_name+".Photon_DR_Muon",0.4);
    m_DR_jet_e  = conf.getNum(m_name+".Jet_DR_Electron",0.3);
    m_DR_jet_mu   = conf.getNum(m_name+".Jet_DR_Muon",0.3);
    m_DR_jet_y = conf.getNum(m_name+".Jet_DR_Photon",0.3);
    
    return EL::StatusCode::SUCCESS;
  }
  
  // Remove overlap. The input containers are modified: overlapping elements are removed
  void MyOverlap::mremoveOverlap(xAOD::PhotonContainer &photons,
                                            xAOD::JetContainer &jets,
                                            xAOD::ElectronContainer &elecs,
                                            xAOD::MuonContainer &muons)
  {
    mremoveOverlap(&photons,&jets,&elecs,&muons);
  }
  
  // Remove overlap. The input containers are modified: overlapping elements are removed
  void MyOverlap::mremoveOverlap(xAOD::PhotonContainer *photons,
                                            xAOD::JetContainer *jets,
                                            xAOD::ElectronContainer *elecs,
                                            xAOD::MuonContainer *muons)
  {

    // 1. remove electrons overlapping with muons
    if (elecs!=nullptr) mremoveOverlap(*elecs,*muons,m_DR_e_mu);
    
    // 2. jets
    if (jets!=nullptr) {
      
      // 2.a remove jets overlapping with photons
      mremoveOverlap(*jets,*photons,m_DR_jet_y);

      // 2.b remove jets overlapping with electrons
      if (elecs!=nullptr) mremoveOverlap(*jets,*elecs,m_DR_jet_e);
      // 2.c remove jets overlapping with muons
      if (muons!=nullptr) mremoveOverlap(*jets,*muons,m_DR_jet_mu);

    }

    if (photons==nullptr) HG::fatal("removeOverlap cannot be done without photons!");
    if (muons!=nullptr) mremoveOverlap(*photons,*muons,m_DR_y_mu);
    if (elecs!=nullptr) mremoveOverlap(*photons,*elecs,m_DR_y_e);
    
  }

}
