#include <ZnunuGamVBS_pack/ZnunuGamVBS.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif


#ifdef __CINT__
#pragma link C++ class ZnunuGamVBS+;
#endif

#ifdef __CINT__
#pragma link C++ class MyOverlap+;
#endif
