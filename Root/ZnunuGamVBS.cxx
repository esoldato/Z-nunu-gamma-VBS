#include "ZnunuGamVBS_pack/ZnunuGamVBS.h"
#include "HGamAnalysisFramework/HGamCommon.h"
#include "HGamAnalysisFramework/HGamVariables.h"

// this is needed to distribute the algorithm to the workers
ClassImp(ZnunuGamVBS)



ZnunuGamVBS::ZnunuGamVBS(const char *name)
: HgammaAnalysis(name)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



ZnunuGamVBS::~ZnunuGamVBS()
{
  // Here you delete any memory you allocated during your analysis.
}



EL::StatusCode ZnunuGamVBS::createOutput()
{
  // Here you setup the histograms needed for you analysis. This method
  // gets called after the Handlers are initialized, so that the systematic
  // registry is already filled.

  TFile *file = wk()->getOutputFile("MxAOD");


sum_of_weights=0;sum_of_weights1=0;

// Tree gamma+met
  m_tree = new TTree("output_tree","output_tree");
  m_tree->SetDirectory(file);
// Tree ee for fr calc
  m_fr_ee_tree = new TTree("output_tree_fr_ee","output_tree_fr_ee");
  m_fr_ee_tree->SetDirectory(file);
// Tree egamma for fr calc
  m_fr_eg_tree = new TTree("output_tree_fr_eg","output_tree_fr_eg");
  m_fr_eg_tree->SetDirectory(file);
// Tree e+met
  m_emet_tree = new TTree("output_tree_emet","output_tree_emet");
  m_emet_tree->SetDirectory(file);
// Tree with truth info gamma+met
        m_tr_tree = new TTree("output_tr_tree","output_tr_tree");
        m_tr_tree->SetDirectory(file);
// Sum of weights
        m_tree_sw = new TTree("output_tree_sw","output_tree_sw");
        m_tree_sw->SetDirectory(file);      
// Events
        m_tree_ev = new TTree("output_tree_ev","output_tree_ev");
        m_tree_ev->SetDirectory(file);   

// Branches

//events
  m_tree_ev->Branch("weight", &Weight);
//sum of weights
  m_tree_sw->Branch("sum_of_weights", &sum_of_weights); // sum-of-weights for events in DxAOD after pileup weighting, and using any non-standard nominal weight
  m_tree_sw->Branch("sum_of_weights_bk_DxAOD", &m_sumw_DxAOD); // sum-of-weights in DxAOD book keeper (only includes default MC weights)
  m_tree_sw->Branch("sum_of_weights_bk_xAOD", &m_sumw_xAOD); // sum-of-weights in xAOD book keeper (only includes default MC weights)

//e+met
  m_emet_tree->Branch("n_ph", &n_ph); //leading el
  m_emet_tree->Branch("n_jet", &n_jet); //number of jets in event
  m_emet_tree->Branch("n_e_looseBL", &n_e_looseBL);  //number of electrons in event
  m_emet_tree->Branch("n_e_medium", &n_e_medium);  //number of electrons in event
  m_emet_tree->Branch("n_mu", &n_mu); //number of muons in event

  m_emet_tree->Branch("weight", &Weight); //there's no need in photon scale factors forelectron, thus using Weight instead weight_cor

  m_emet_tree->Branch("el_pt", &e_lead_pt);  //leading el p_t
  m_emet_tree->Branch("el_eta", &e_lead_eta);  //eta
  m_emet_tree->Branch("el_phi", &e_lead_phi);  //phi
  m_emet_tree->Branch("el_E", &e_lead_E);  //E

  m_emet_tree->Branch("jet_sum_pt", &jet_sum_pt);  //sum jet p_t
  m_emet_tree->Branch("jet_sphericity", jet_sphericity);  //jet spher

  m_emet_tree->Branch("jet_lead_pt", &jet_lead_pt);  //leading jet p_t
  m_emet_tree->Branch("jet_lead_eta", &jet_lead_eta);  //eta 
  m_emet_tree->Branch("jet_lead_phi", &jet_lead_phi);  //phi
  m_emet_tree->Branch("jet_lead_E", &jet_lead_E);    //E

  m_emet_tree->Branch("jet_sublead_pt", &jet_sublead_pt);  //subleading jet p_t
  m_emet_tree->Branch("jet_sublead_eta", &jet_sublead_eta);  //eta 
  m_emet_tree->Branch("jet_sublead_phi", &jet_sublead_phi);  //phi
  m_emet_tree->Branch("jet_sublead_E", &jet_sublead_E);    //E

  m_emet_tree->Branch("metTST_pt", &metTST_pt);  //TST MET p_t
  m_emet_tree->Branch("metTST_phi", &metTST_phi);  //phi  

  m_emet_tree->Branch("metCST_pt", &metCST_pt);  //CST MET p_t
  m_emet_tree->Branch("metCST_phi", &metCST_phi);  //phi  

  m_emet_tree->Branch("mc_weight", &tr_Weight); 


//gamma+met
  m_tree->Branch("n_ph", &n_ph); //number of at least loose photons. before: either number of Tight photons with pT > 150 GeV (if tight_photons = true) or number of all photons which pass preselection (if tight_photons = false)
  m_tree->Branch("n_jet", &n_jet); //number of jets in event
  m_tree->Branch("n_e_looseBL", &n_e_looseBL);  //number of electrons in event
  m_tree->Branch("n_e_medium", &n_e_medium);  //number of electrons in event
  m_tree->Branch("n_mu", &n_mu); //number of muons in event
  m_tree->Branch("weight", &weight_cor); 
        m_tree->Branch("RunNumber", &RunNumber);
        m_tree->Branch("EventNumber", &EventNumber);

  m_tree->Branch("ph_pt", &ph_pt);  //leading at least loose photon p_t
  m_tree->Branch("ph_eta", &ph_eta);  //eta
  m_tree->Branch("ph_phi", &ph_phi);  //phi

  m_tree->Branch("ph_iso_et40", &ph_iso_et40);    //topoetcone40
  m_tree->Branch("ph_iso_et20", &ph_iso_et20);    //topoetcone20
  m_tree->Branch("ph_iso_pt", &ph_iso_pt);    //ptcone20
  m_tree->Branch("ph_isem",&m_ph_isEM_tight_mc15);

  m_tree->Branch("ph_z_point",&ph_z_pointed);

  m_tree->Branch("ph_convFlag",&ph_convFlag);
  m_tree->Branch("mc_ph_origin",&mc_ph_origin);
  m_tree->Branch("mc_ph_type",&mc_ph_type);

  m_tree->Branch("jet_sum_pt", &jet_sum_pt);  //sum jet p_t
  m_tree->Branch("jet_sphericity", &jet_sphericity);  //jet spher

  m_tree->Branch("jet_lead_pt", &jet_lead_pt);  //leading jet p_t
  m_tree->Branch("jet_lead_eta", &jet_lead_eta);  //eta 
  m_tree->Branch("jet_lead_phi", &jet_lead_phi);  //phi
  m_tree->Branch("jet_lead_E", &jet_lead_E);    //E

  m_tree->Branch("jet_sublead_pt", &jet_sublead_pt);  //subleading jet p_t
  m_tree->Branch("jet_sublead_eta", &jet_sublead_eta);  //eta 
  m_tree->Branch("jet_sublead_phi", &jet_sublead_phi);  //phi
  m_tree->Branch("jet_sublead_E", &jet_sublead_E);    //E

  m_tree->Branch("metTST_pt", &metTST_pt);  //TST MET p_t
  m_tree->Branch("metTST_phi", &metTST_phi);  //phi  

  m_tree->Branch("metCST_pt", &metCST_pt);  //CST MET p_t
  m_tree->Branch("metCST_phi", &metCST_phi);  //phi 

  m_tree->Branch("mc_weight", &tr_Weight); 

//fr ee
  m_fr_ee_tree->Branch("e_lead_pt", &e_lead_pt);  //leading electron p_t
  m_fr_ee_tree->Branch("e_lead_eta", &e_lead_eta);  //eta
  m_fr_ee_tree->Branch("e_lead_phi", &e_lead_phi);  //phi
  m_fr_ee_tree->Branch("e_lead_E", &e_lead_E);    //E

  m_fr_ee_tree->Branch("e_sublead_pt", &e_sublead_pt);  //subleading electron p_t
  m_fr_ee_tree->Branch("e_sublead_eta", &e_sublead_eta);  //eta
  m_fr_ee_tree->Branch("e_sublead_phi", &e_sublead_phi);  //phi
  m_fr_ee_tree->Branch("e_sublead_E", &e_sublead_E);    //E
  m_fr_ee_tree->Branch("metTST_pt", &metTST_pt);  //TST MET p_t
  m_fr_ee_tree->Branch("n_ph", &n_ph);

//fr eg
  m_fr_eg_tree->Branch("e_lead_pt", &e_lead_pt);  //leading electron p_t
  m_fr_eg_tree->Branch("e_lead_eta", &e_lead_eta);  //eta
  m_fr_eg_tree->Branch("e_lead_phi", &e_lead_phi);  //phi
  m_fr_eg_tree->Branch("e_lead_E", &e_lead_E);    //E

  m_fr_eg_tree->Branch("ph_pt", &ph_pt);  //subleading electron p_t
  m_fr_eg_tree->Branch("ph_eta", &ph_eta);  //eta
  m_fr_eg_tree->Branch("ph_phi", &ph_phi);  //phi
  m_fr_eg_tree->Branch("ph_convFlag",&ph_convFlag);
  m_fr_eg_tree->Branch("metTST_pt", &metTST_pt);  //TST MET p_t
  m_fr_eg_tree->Branch("ph_iso_et40", &ph_iso_et40);    //topoetcone40
  m_fr_eg_tree->Branch("ph_iso_et20", &ph_iso_et20);    //topoetcone20
  m_fr_eg_tree->Branch("ph_iso_pt", &ph_iso_pt);    //ptcone20
  m_fr_eg_tree->Branch("ph_isem",&m_ph_isEM_tight_mc15);

  m_fr_eg_tree->Branch("ph_z_point",&ph_z_pointed);


//truth
        m_tr_tree->Branch("tr_ph_pt", &tr_ph_pt);  //leading at least loose ph p_t
        m_tr_tree->Branch("tr_ph_eta", &tr_ph_eta);  //eta
        m_tr_tree->Branch("tr_ph_phi", &tr_ph_phi);  //phi
        m_tr_tree->Branch("tr_ph_n", &tr_ph_n);    //number
        m_tr_tree->Branch("tr_ph_ptcone20", &tr_ph_ptcone20);    //ptcone20
        m_tr_tree->Branch("tr_ph_etcone40", &tr_ph_etcone40);    //etcone40

        m_tr_tree->Branch("tr_Z_pt", &tr_Z_pt);  //nunu tr system p_t
        m_tr_tree->Branch("tr_Z_phi", &tr_Z_phi);  //phi

        m_tr_tree->Branch("tr_jet_pt", &tr_jet_pt);  //leading tr jet p_t
        m_tr_tree->Branch("tr_jet_eta", &tr_jet_eta);  //eta
        m_tr_tree->Branch("tr_jet_phi", &tr_jet_phi);  //phi
        m_tr_tree->Branch("tr_jet_E", &tr_jet_E);    //E
        m_tr_tree->Branch("tr_sl_jet_pt", &tr_sl_jet_pt);  //leading tr jet p_t
        m_tr_tree->Branch("tr_sl_jet_eta", &tr_sl_jet_eta);  //eta
        m_tr_tree->Branch("tr_sl_jet_phi", &tr_sl_jet_phi);  //phi
        m_tr_tree->Branch("tr_sl_jet_E", &tr_sl_jet_E);    //E
        m_tr_tree->Branch("tr_jet_n", &tr_jet_n);    //number
  m_tr_tree->Branch("tr_jet_sum_pt", &tr_jet_sum_pt);  //sum jet p_t

        m_tr_tree->Branch("tr_el_n", &tr_el_n);    //number true el
        m_tr_tree->Branch("tr_mu_n", &tr_mu_n);    //number true mu
  m_tr_tree->Branch("tr_weight", &tr_Weight);

//histograms
  histoStore()->createTH1F("pT_lead_y", 100, 0, 1000,";#it{p}_{T#gamma} [GeV]");  //leading photon p_T
  histoStore()->createTH1F("pT_MET_TST", 100, 0, 1000,";#it{p}^{miss}_{T} [GeV]");  //missing p_T
  histoStore()->createTH1F("pT_MET_CST", 100, 0, 1000,";#it{p}^{miss}_{T} [GeV]");  //missing p_T

  histoStore()->createTH1F("dPhi_y_MET", 70, 0, 7,";d#phi(#gamma,p^{miss}_{T})"); //dPhi between leading photon and MET
  histoStore()->createTH1F("dPhi_jet_MET", 70, 0, 7,";d#phi(jet,p^{miss}_{T})");  //dPhi between leading jet and MET
  histoStore()->createTH1F("dPhi_y_jet", 70, 0, 7,";d#phi(jet,#gamma)");  //dPhi between leading jet and leading photon

        histoStore()->createTH1F("n_ph", 10, 0, 10,";n_#gamma"); 
        histoStore()->createTH1F("n_ph1", 10, 0, 10,";n_#gamma");

        histoStore()->createTH1F("n_el", 10, 0, 10,";n_el");
        histoStore()->createTH1F("n_el1", 10, 0, 10,";n_el");

        histoStore()->createTH1F("n_jet", 10, 0, 10,";n_jet");
        histoStore()->createTH1F("n_jet1", 10, 0, 10,";n_jet");
        histoStore()->createTH1F("n_jet2", 10, 0, 10,";n_jet");

        histoStore()->createTH1F("ph_parent_pdgid", 100, 0, 100,";id");
        histoStore()->createTH1F("ph_barcode", 250, 0, 250000,";barcode");

        histoStore()->createTH1F("ne_parent_pdgid", 100, 0, 100,";id");
        histoStore()->createTH1F("ne_barcode", 250, 0, 250000,";barcode");

  histoStore()->createTH1F("cut_flow_ph", 3, 0, 3,";");
  histoStore()->getTH1F("cut_flow_ph")->SetCanExtend(TH1::kAllAxes);
  histoStore()->createTH1F("cut_flow_el", 3, 0, 3,";");
  histoStore()->getTH1F("cut_flow_el")->SetCanExtend(TH1::kAllAxes);
  histoStore()->createTH1F("cut_flow_mu", 3, 0, 3,";");
  histoStore()->getTH1F("cut_flow_mu")->SetCanExtend(TH1::kAllAxes);
  histoStore()->createTH1F("cut_flow_jet", 3, 0, 3,";");
  histoStore()->getTH1F("cut_flow_jet")->SetCanExtend(TH1::kAllAxes);
  histoStore()->createTH1F("cut_flow_ev", 3, 0, 3,";");
  histoStore()->getTH1F("cut_flow_ev")->SetCanExtend(TH1::kAllAxes);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ZnunuGamVBS::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // Important to keep this, so that internal tools / event variables
  // are filled properly.
  HgammaAnalysis::execute();
  // histoStore()->fillTH1F("cut_flow", 0); 
  Weight=1;
  // std::cout<<"execute1"<<std::endl;
  if (HG::isMC()==true) {
    Weight = eventHandler()->mcWeight();
    tr_Weight = eventHandler()->mcWeight();
    Weight *= eventHandler()->pileupWeight();
    /// Weight *= eventHandler()->vertexWeight(); std::cout << "Vertex weight: "<<  eventHandler()->vertexWeight()<< std::endl;
    if(sum_of_weights1==0 && sum_of_weights!=0){m_tree_sw->Fill();}
    sum_of_weights1 += Weight;
  }
  m_tree_ev->Fill();
  histoStore()->getTH1F("cut_flow_ev")->Fill("all events", Weight); 

  n_ph = 0; n_e_looseBL = 0; n_e_medium = 0; n_mu = 0; n_jet = 0, RunNumber=0, EventNumber=0;
  ph_pt = 0; ph_eta = -10; ph_phi = -10; ph_iso_et40 = 0; ph_iso_et20 = 0; ph_iso_pt = 0;
  tr_ph_pt = 0; tr_ph_eta = -10; tr_ph_phi = -10; tr_ph_n=0;  tr_ph_etcone40=-10;  tr_ph_ptcone20=-10;
  tr_Z_pt = 0; tr_Z_phi = -10; tr_Z_n=0;
  tr_el_n = 0; tr_mu_n = 0;
  tr_jet_pt = 0; tr_jet_eta = -10; tr_jet_phi = -10; tr_jet_E = 0; tr_jet_n=0;
  tr_sl_jet_pt = 0; tr_sl_jet_eta = -10; tr_sl_jet_phi = -10; tr_sl_jet_E = 0;
  e_lead_pt = 0; e_lead_eta = -10; e_lead_phi = -10; e_lead_E = 0;
  e_sublead_pt = 0; e_sublead_eta = -10; e_sublead_phi = -10; e_sublead_E = 0;
  jet_lead_pt = 0; jet_lead_eta = -10; jet_lead_phi = -10; jet_lead_E = 0;
  jet_sublead_pt = 0; jet_sublead_eta = -10; jet_sublead_phi = -10; jet_sublead_E = 0;
  metCST_pt = 0; metCST_phi = -10;
  metTST_pt = 0; metTST_phi = -10; n_tau=0;
  double pt=0;

  double lead_ph_iso_et40=-10;
  double lead_ph_iso_et20=-10;
  double lead_ph_iso_pt=-10;
  double jet_t_xx,jet_t_xy,jet_t_yy;

  ph_convFlag=-1; ph_tight=false; m_ph_isEM_tight_mc15=-1;

  if (!eventHandler()->pass()) return EL::StatusCode::SUCCESS;
  histoStore()->getTH1F("cut_flow_ev")->Fill("eventHandler pass", Weight); 
  
  RunNumber = eventInfo()->runNumber();
  EventNumber = eventInfo()->eventNumber();
  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();
  
  xAOD::PhotonContainer AllPhotons = photonHandler()->getCorrectedContainer(); //corrected container with author cut (if applyed)
  histoStore()->getTH1F("cut_flow_ph")->Fill("all photons", AllPhotons.size()); 
  xAOD::PhotonContainer presel_no_loose_photons = photonHandler()->applySelection(AllPhotons); //preselection of photons without overlap, Tightness and isolation criteria
  histoStore()->getTH1F("cut_flow_ph")->Fill("basic cuts", presel_no_loose_photons.size()); 
  xAOD::PhotonContainer presel_photons(SG::VIEW_ELEMENTS);
  xAOD::PhotonContainer sel_photons(SG::VIEW_ELEMENTS);
  xAOD::PhotonContainer check_photon(SG::VIEW_ELEMENTS);

  for (auto pho: presel_no_loose_photons){                           //

      const char clean = pho->auxdata<char>("DFCommonPhotonsCleaning");
      if (!clean) continue;
      check_photon.push_back(pho);
      const char PID = pho->auxdata<char>("DFCommonPhotonsIsEMLoose");
      if (!PID) continue;
      presel_photons.push_back(pho);
      if (fabs(pho->caloCluster()->etaBE(2))>1.37 && fabs(pho->caloCluster()->etaBE(2))<1.52) continue;
      if ((pho->pt())*HG::invGeV<150) continue;
      sel_photons.push_back(pho);
                                            //
    }
    histoStore()->getTH1F("cut_flow_ph")->Fill("cleaning", check_photon.size()); 
    histoStore()->getTH1F("cut_flow_ph")->Fill("loose ID", presel_photons.size()); 
    histoStore()->getTH1F("cut_flow_ph")->Fill("crack region and pT", sel_photons.size()); 

///////////////////////////because setSelectedObjects() gives wrong trigger SF, only iso and ID SFs for photons are used
    double weightSF=1;
    if (HG::isMC()) {
      static SG::AuxElement::Accessor<float> scaleFactor("scaleFactor");

      if (sel_photons.size()!=0) {
        for (auto photon: sel_photons)
        { weightSF *= scaleFactor(*photon);}
      }
    }
    // std::cout<<"weightSF: "<<weightSF<<"\n";
    weight_cor=weightInitial()*weightSF;
// setSelectedObjects(&sel_photons);
// weight_cor=weight();
//////////////////////////

  xAOD::ElectronContainer AllElectrons = electronHandler()->getCorrectedContainer(); //corrected electron container
  histoStore()->getTH1F("cut_flow_el")->Fill("all electrons", AllElectrons.size()); 
  xAOD::ElectronContainer sel_electrons = electronHandler()->applySelection(AllElectrons); //preselection of electrons //no PID
  xAOD::ElectronContainer sel_elec_medium(SG::VIEW_ELEMENTS);              
  xAOD::ElectronContainer sel_elec_looseBL(SG::VIEW_ELEMENTS);              
  xAOD::ElectronContainer sel_elec_QQ(SG::VIEW_ELEMENTS);
  xAOD::ElectronContainer sel_elec_looseBL_Zpeak(SG::VIEW_ELEMENTS);  
  xAOD::ElectronContainer sel_elec_looseBL_noiso(SG::VIEW_ELEMENTS);             
  xAOD::ElectronContainer sel_elec_looseBL_iso(SG::VIEW_ELEMENTS);             
  histoStore()->getTH1F("cut_flow_el")->Fill("basic sel", sel_electrons.size()); 

  for (auto ele: sel_electrons){
    if (!ele->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON)) continue;

    sel_elec_QQ.push_back(ele);
    const char PID = ele->auxdata<char>("DFCommonElectronsLHLooseBL");
    if (!PID) continue;
    sel_elec_looseBL_noiso.push_back(ele);
    if (!electronHandler()->passIsoCut(ele)) continue;
    sel_elec_looseBL_iso.push_back(ele);
    if (fabs(ele->caloCluster()->etaBE(2))>1.37 && fabs(ele->caloCluster()->etaBE(2))<1.52) continue;
    sel_elec_looseBL.push_back(ele); 
    const char PID_medium = ele->auxdata<char>("DFCommonElectronsLHMedium");
    if (PID_medium) {sel_elec_medium.push_back(ele);}
    if ((ele->caloCluster()->pt())*HG::invGeV<25) continue;
    sel_elec_looseBL_Zpeak.push_back(ele);
  }
  histoStore()->getTH1F("cut_flow_el")->Fill("QQ", sel_elec_QQ.size()); 
  histoStore()->getTH1F("cut_flow_el")->Fill("loose ID", sel_elec_looseBL_noiso.size()); 
  histoStore()->getTH1F("cut_flow_el")->Fill("isolation", sel_elec_looseBL_iso.size()); 
  histoStore()->getTH1F("cut_flow_el")->Fill("crack region", sel_elec_looseBL.size()); 
  histoStore()->getTH1F("cut_flow_el")->Fill("medium ID", sel_elec_medium.size()); 
  histoStore()->getTH1F("cut_flow_el")->Fill("pT cut (for loose el)", sel_elec_looseBL_Zpeak.size()); 

  xAOD::MuonContainer AllMuons = muonHandler()->getCorrectedContainer(); //corrected muon container
  xAOD::MuonContainer sel_muons(SG::VIEW_ELEMENTS); //preselection of muons
  histoStore()->getTH1F("cut_flow_mu")->Fill("all muons", AllMuons.size()); 
  xAOD::MuonContainer sel_muons_noiso = muonHandler()->applySelection(AllMuons); //preselection of muons
  histoStore()->getTH1F("cut_flow_mu")->Fill("preselected muons", sel_muons_noiso.size()); 

  for (auto muo: sel_muons_noiso){
    if (!muonHandler()->passIsoCut(muo)) continue;
    sel_muons.push_back(muo);
  } 
  histoStore()->getTH1F("cut_flow_mu")->Fill("isolation", sel_muons.size()); 

  xAOD::JetContainer AllJets = jetHandler()->getCorrectedContainer(); //corrected container of jets
  histoStore()->getTH1F("cut_flow_jet")->Fill("all jets", AllJets.size()); 
  
  //////Missing transverse energy

  xAOD::MissingETContainer met_reb = etmissHandler()->getCorrectedContainer(&presel_photons, &AllJets, &sel_elec_looseBL_noiso, &sel_muons_noiso);
  const xAOD::MissingET* met_reb_TST1 = (met_reb)["TST"];
   if (!met_reb_TST1)
   {
       Error("execute()","Failed to retrieve met_TST container from HGamAnalysisFramework handler. Exiting.");
       return EL::StatusCode::SUCCESS;
   }
 /* Removed since new tool can't store two different soft terms in same container
   const xAOD::MissingET* met_reb_CST1 = (met_reb)["CST"];
   if (!met_reb_CST1)
   {
       Error("execute()","Failed to retrieve met_CST container from HGamAnalysis handler. Exiting.");
       return EL::StatusCode::SUCCESS;
   }*/
    
   TLorentzVector mmTST,mmCST,mmRef;
   mmTST.SetPxPyPzE(met_reb_TST1->mpx(), met_reb_TST1->mpy(), 0 , met_reb_TST1->met());
   mmTST *= HG::invGeV;
   // mmCST.SetPxPyPzE(met_reb_CST1->mpx(), met_reb_CST1->mpy(), 0 , met_reb_CST1->met());
   // mmCST *= HG::invGeV;


   histoStore()->fillTH1F("pT_MET_TST",mmTST.Pt());
   // histoStore()->fillTH1F("pT_MET_CST",mmCST.Pt());
   metTST_pt = mmTST.Pt();  //MET TST TLorentzVector 
   metTST_phi = mmTST.Phi();  //
   // metCST_pt = mmCST.Pt();  //MET CST TLorentzVector 
   // metCST_phi = mmCST.Phi();  //

  ////// jet preselection
  xAOD::JetContainer selected_jets_noJVT = jetHandler()->applySelectionNoJvt(AllJets);
  histoStore()->getTH1F("cut_flow_jet")->Fill("selected", selected_jets_noJVT.size()); 
  xAOD::JetContainer selected_jets(SG::VIEW_ELEMENTS);

  //Event cleaning
  const char cleanJetEvent = eventInfo()->auxdata<char>("DFCommonJets_eventClean_LooseBad");
  if (!cleanJetEvent) return EL::StatusCode::SUCCESS;
  histoStore()->getTH1F("cut_flow_ev")->Fill("jet cleaning", Weight); 

  overlapMine()->mremoveOverlap(sel_photons, selected_jets_noJVT, sel_elec_looseBL, sel_muons); //overlap removal

  histoStore()->getTH1F("cut_flow_jet")->Fill("OR", selected_jets_noJVT.size()); 
  histoStore()->getTH1F("cut_flow_ph")->Fill("OR", sel_photons.size()); 
  histoStore()->getTH1F("cut_flow_el")->Fill("OR", sel_elec_looseBL.size()); 
  histoStore()->getTH1F("cut_flow_mu")->Fill("OR", sel_muons.size()); 

  for(auto jet_itr: selected_jets_noJVT){
    if (jetHandler()->passJVTCut(jet_itr)) 
      selected_jets.push_back(jet_itr);
  }

  histoStore()->getTH1F("cut_flow_jet")->Fill("JVT", selected_jets.size()); 

  pt=0;
  vec=TLorentzVector(); 
  lead_jet=TLorentzVector();
  jet_sum_pt=0;jet_t_xx=0;jet_t_xy=0;jet_t_yy=0;jet_sphericity=0;

  ///////looking for leading jet
  if (selected_jets.size()!=0){
    for(auto jet_itr: selected_jets) {  
  jet_sum_pt=jet_sum_pt+jet_itr->p4().Pt();
  jet_t_xx=jet_t_xx+jet_itr->p4().Px()*jet_itr->p4().Px()/jet_itr->p4().P()/jet_itr->p4().P();
  jet_t_xy=jet_t_xy+jet_itr->p4().Px()*jet_itr->p4().Py()/jet_itr->p4().P()/jet_itr->p4().P();
  jet_t_yy=jet_t_yy+jet_itr->p4().Py()*jet_itr->p4().Py()/jet_itr->p4().P()/jet_itr->p4().P();
      vec = jet_itr->p4();
        if (vec.Pt() > pt){ 
          pt = vec.Pt();
          lead_jet = vec;
        }
    }
  }
jet_sphericity=jet_t_xx*jet_t_yy-jet_t_xy*jet_t_xy;
jet_sum_pt *= HG::invGeV;
  //////looking for subleading jet
  pt=0;
  if (selected_jets.size()>1){
    for(auto jet_itr: selected_jets) {
      vec = jet_itr->p4();
        if (vec != lead_jet && vec.Pt() > pt){ 
          pt = vec.Pt();
          vec *= HG::invGeV;
          jet_sublead_pt = vec.Pt();
          jet_sublead_eta = vec.Eta();
          jet_sublead_phi = vec.Phi();
          jet_sublead_E = vec.E();
        }
    }
  }
  lead_jet *= HG::invGeV; // convert these 4-vectors to GeV
  n_jet = selected_jets.size();
  jet_lead_pt = lead_jet.Pt();
  jet_lead_eta = lead_jet.Eta();
  jet_lead_phi = lead_jet.Phi();
  jet_lead_E = lead_jet.E();

  /// fill tree number of leptons in event
  n_e_medium = sel_elec_medium.size();
  n_e_looseBL = sel_elec_looseBL.size();
  n_e_looseBL_ptcut = sel_elec_looseBL_Zpeak.size();
  n_mu = sel_muons.size();
  
  ////////electons for electron to photon fake rate
  pt = 0; double sign=0;
  vec=lead_el=TLorentzVector();
  emet=false; fr_ee=false; fr_ey=false;

  if (n_e_looseBL!=0) {

    if (n_e_looseBL>=1) { 
      for (auto el: sel_elec_looseBL){
        vec = el->p4();
        vec *= HG::invGeV;
        if (vec.Pt()>pt) {pt=vec.Pt(); lead_el = vec; }
      }
//        vec = sel_elec_looseBL[0]->p4();
//        vec *= HG::invGeV;
          e_lead_E = lead_el.E();
          e_lead_pt = lead_el.Pt();
          e_lead_eta = lead_el.Eta();
          e_lead_phi = lead_el.Phi();
  emet=true;
    }
  pt = 150;vec=lead_el=TLorentzVector();
    if (n_e_looseBL_ptcut==2) {
      for (auto el: sel_elec_looseBL_Zpeak){
        vec = el->p4();
        vec *= HG::invGeV;
        if (vec.Pt()>pt) {pt=vec.Pt(); lead_el = vec; sign = el->charge();}
      }
      for (auto el: sel_elec_looseBL_Zpeak){
        vec = el->p4();
        vec *= HG::invGeV;
        if (vec!=lead_el && sign != el->charge()) {

          e_lead_E = lead_el.E();
          e_lead_pt = lead_el.Pt();
          e_lead_eta = lead_el.Eta();
          e_lead_phi = lead_el.Phi();

          e_sublead_E = vec.E();
          e_sublead_pt = vec.Pt();
          e_sublead_eta = vec.Eta();
          e_sublead_phi = vec.Phi();

          fr_ee = true;
        }
      }
    }
  }
  ////////////
  if (sel_photons.size()==0) {if(fr_ee && lead_el.Pt()>150 && HG::isMC()==false){m_fr_ee_tree->Fill(); /*return EL::StatusCode::SUCCESS;*/} 
  if(e_lead_pt>150 && metTST_pt>100 && emet){m_emet_tree->Fill();}}

      lead_ph=TLorentzVector();
      vec=TLorentzVector();
      pt=150;n_ph_tight=0;n_ph_all=0;
      for (auto ph: sel_photons){ 
        vec = ph->p4(); vec *= HG::invGeV; 
        if (vec.Pt() > pt) {
          check_photon.clear();
          pt = vec.Pt();
          lead_ph = vec;
          lead_ph_iso_et40 = ph->isolationValue(xAOD::Iso::topoetcone40)*HG::invGeV;
          lead_ph_iso_et20 = ph->isolationValue(xAOD::Iso::topoetcone20)*HG::invGeV;
          lead_ph_iso_pt = ph->isolationValue(xAOD::Iso::ptcone20)*HG::invGeV;
          check_photon.push_back(ph);   
          ph_convFlag = xAOD::EgammaHelpers::conversionType(ph);
          // ph_tight = ph->auxdata<char>("DFCommonPhotonsIsEMTight"); //if someone needs Tight ID only...
          m_ph_isEM_tight_mc15 = ph->auxdata<unsigned int>("DFCommonPhotonsIsEMTightIsEMValue");
          if (ph_tight && lead_ph_iso_et40<(2.45+0.022*lead_ph.Pt()) && lead_ph_iso_pt/lead_ph.Pt()<0.05) {n_ph_tight++;}
          n_ph_all++;
          // std::cout<<"execute2"<<std::endl;
          if (HG::isMC()) {
            mc_ph_origin = ph->auxdata< int >( "truthOrigin" );
            mc_ph_type = ph->auxdata< int >( "truthType" );
          }
        }
      }
    ph_iso_et40= lead_ph_iso_et40;
    ph_iso_et20= lead_ph_iso_et20;
    ph_iso_pt= lead_ph_iso_pt;
    ph_pt = lead_ph.Pt();
    ph_eta = lead_ph.Eta();
    ph_phi = lead_ph.Phi();
    n_ph = sel_photons.size();


    if (pointTool->updatePointingAuxdata(check_photon).isFailure()) {
        HG::fatal("Couldn't update photon calo pointing auxdata");
      }
    double zPointed = xAOD::PVHelpers::getZCommonAndError(eventInfo(), &check_photon).first;
    double pv_z = eventHandler()->hardestVertexZ();
    ph_z_pointed=zPointed-pv_z;

    if (n_ph>0) {
      histoStore()->getTH1F("cut_flow_ev")->Fill("n_ph>0", Weight); 
      if (metTST_pt>130) {
        histoStore()->getTH1F("cut_flow_ev")->Fill("MET>130 GeV", Weight); 
        if (ph_pt>150){
          histoStore()->getTH1F("cut_flow_ev")->Fill("ph_pT>150 GeV", Weight); 
          m_tree->Fill();
        }
      }
    }
    if (n_e_looseBL_ptcut==1 && n_ph_all>0) fr_ey = true; //flag for egamma events for fake rate
    if (fr_ey){if(HG::isMC()==false){m_fr_eg_tree->Fill();}} 

  m_store->clear();
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZnunuGamVBS::setupJob (EL::Job& job)
{
  HgammaAnalysis::setupJob(job); // keep this line!
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZnunuGamVBS::fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  HgammaAnalysis::fileExecute(); // keep this line!

  Weight=0;
///std::cout << "photon_parent=" << wk()->xaodEvent()->getEntries() << std::endl;
  TTree *MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
  if (!MetaData) {
    Error("fileExecute()", "MetaData not found! Exiting.");
    return EL::StatusCode::FAILURE;
  }
  
  bool isAOD = MetaData->GetBranch("StreamAOD");
  bool isMAOD = !MetaData->GetBranch("TriggerMenu");
  HG::setAndLock_InputType(isAOD,isMAOD);

  MetaData->LoadTree(0);

  const xAOD::EventInfo *eventInfo = 0;

  if (wk()->xaodEvent()->retrieve(eventInfo, "EventInfo").isFailure()) {
    HG::fatal("Cannot access EventInfo");
  }

  HG::setAndLock_isMC(eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION));

if (wk()->xaodEvent()->getEntries()!=0){
  // std::cout<<"file execute"<<std::endl;
if ( HG::isMC()==true){
  if (!HG::isAOD()&&!HG::isMAOD()) {
    // If we get here, this is a DxAOD

    // 1. Check if there if the incomplete book-keeper object has entreies (that would be bad!)
    const xAOD::CutBookkeeperContainer* incompleteCBC = nullptr;
    EL_CHECK( "fileExecute()", wk()->xaodEvent()->retrieveMetaInput(incompleteCBC,"IncompleteCutBookkeepers") );
    if ( incompleteCBC->size() != 0 ) {
      // fatal or error? let's start with a hard fatal!
      // HG::fatal("Issue with DxAOD book keeper. It's incomplete. File corrupted?");
      Warning("fileexecute()",
        "Issue with DxAOD book keeper. It's incomplete. File corrupted? %s %s",
        "If this is data, this is known to happen (but not understood).",
              "If this is MC, this is expected to happen, and can probably be ignored.");
    }

    // 2. now get the actual bookkeeper
    const xAOD::CutBookkeeperContainer* completeCBC = nullptr;
    EL_CHECK( "fileExecute()", wk()->xaodEvent()->retrieveMetaInput(completeCBC,"CutBookkeepers") );

    int maxAcycle = -1, maxDcycle = -1;
    for ( auto cbk : *completeCBC ) {
      Info("fileExecute()","  Book keeper name=%s, inputStream=%s, cycle=%d, nAcceptedEvents=%d", cbk->name().c_str(), cbk->inputStream().c_str(), cbk->cycle(), (int)cbk->nAcceptedEvents());

      if ( cbk->name().empty() ) continue;

      // Use the DxAOD numbers from the largest cycle
      if (TString(cbk->name()).Contains("EXOT6") && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxDcycle) {
        maxDcycle     = cbk->cycle();
/// int m_N_DxAOD     = cbk->nAcceptedEvents();
  m_sumw_DxAOD  = cbk->sumOfEventWeights();
/// int m_sumw2_DxAOD = cbk->sumOfEventWeightsSquared();
      }


      // Use the xAOD numbers from the largest cycle
      if (cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxAcycle) {
        maxAcycle    = cbk->cycle();
/// int m_N_xAOD     = cbk->nAcceptedEvents();
  m_sumw_xAOD  = cbk->sumOfEventWeights();
/// int m_sumw2_xAOD = cbk->sumOfEventWeightsSquared();
      }
    }
   // Info("fileExecute()","Book keeper anticipates %i events in current input file (%i in parent xAOD)",m_N_DxAOD,m_N_xAOD);
  }
  sum_of_weights=sum_of_weights1;
  sum_of_weights1=0;
}}  
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZnunuGamVBS::histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  HgammaAnalysis::histInitialize(); // keep this line!

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZnunuGamVBS::changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  HgammaAnalysis::changeInput(firstFile); // keep this line!
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZnunuGamVBS::initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // do not get called if no events are processed.  So any objects
  // you create here will not be available in the output if you have no
  // input events.
  HgammaAnalysis::initialize(); // keep this line!

  m_ph_N = 0;
  m_sel_ph_N = 0;
  m_el_N = 0;
  m_sel_el_N = 0;
  m_mu_N = 0;
  m_sel_mu_N = 0;
  m_jet_N = 0;
  m_sel_jet_N = 0;

  // pointTool = new CP::PhotonPointingTool("PointingTool");
        // pointTool->initialize();
  pointTool = photonHandler()->getPointingTool();

        //m_store = new xAOD::TStore();


  // my_conf=config();
  m_MyOverlap = new HG::MyOverlap();
  // m_MyOverlap->initialize(*my_conf);
  m_MyOverlap->initialize(*config());
  // do_calib=config()->getBool("PhotonHandler.DoCalibAndSmear", true);
  // m_photonTightIsEMSelectorMC15 = new AsgPhotonIsEMSelector( "PhotonTightIsEMSelectorMC15" );
  // CP_CHECK("ZMetGamAnalysis()", m_photonTightIsEMSelectorMC15->setProperty("isEMMask",egammaPID::PhotonTight));
  // CP_CHECK("ZMetGamAnalysis()", m_photonTightIsEMSelectorMC15->setProperty("ConfigFile",config()->getStr("PhotonHandler.Selection.ConfigFile.Tight").Data()));
  // if (!m_photonTightIsEMSelectorMC15->initialize().isSuccess()) {
  //   HG::fatal("Failed to initialize PhotonTightIsEMSelectorMC15");
  // }
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZnunuGamVBS::postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  HgammaAnalysis::postExecute(); // keep this line!
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZnunuGamVBS::finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  HgammaAnalysis::finalize(); // keep this line!
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZnunuGamVBS::histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  if (Weight!=0){
    // std::cout<<"hist finalize"<<std::endl;
    if ( HG::isMC()==true){
      sum_of_weights=sum_of_weights1;
      m_tree_sw->Fill();
    }
  }
  HgammaAnalysis::histFinalize(); // keep this line!
  return EL::StatusCode::SUCCESS;
}

